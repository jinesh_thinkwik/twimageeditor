package com.thinkwik.thinkwikimageeditorlibrary

import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import iamutkarshtiwari.github.io.ananas.editimage.EditImageActivity
import org.shagi.filepicker.ExtFile
import org.shagi.filepicker.FilePicker
import org.shagi.filepicker.FilePickerDialog
import org.shagi.filepicker.FilePickerFragment
import java.io.File

class EditImage(val activity: AppCompatActivity, var folderName: String,var reqCode: Int) {
    val EXTRA_OUTPUT = "extra_output"

    private fun createNewFile(name: String, ext: String): File? {
        try {
            val rootFile =
                File(Environment.getExternalStorageDirectory().toString() + File.separator + folderName + File.separator)
            rootFile.mkdirs()
            val sdImageMainDirectory = File(rootFile, "$name.$ext")
            return sdImageMainDirectory
        } catch (e: Exception) {
            Toast.makeText(
                activity, "Error occured. Please try again later.",
                Toast.LENGTH_SHORT
            ).show()
            return null
        }

    }

    fun pickImage(message: (msg: String) -> Unit, success: (file: File) -> Unit) {
        val pickerFragment = FilePickerFragment.getFragment(activity.supportFragmentManager, false)
        pickerFragment.use(FilePickerDialog.newInstance())
        pickerFragment.setOnLoadingListener(object : FilePicker.OnLoadingListener {
            override fun onLoadingStart(key: Long) {
                message("loading")
            }

            override fun onLoadingSuccess(key: Long, file: ExtFile) {
                success(file.file)

            }

            override fun onLoadingFailure(key: Long, throwable: Throwable) {
                message(throwable.message.toString())
            }

        })
        pickerFragment.show()
    }

    fun editImage(pickedFile: File) {
        val newFile =
            createNewFile(pickedFile.name + "_edited_" + System.currentTimeMillis(), pickedFile.extension.toString())
        EditImageActivity.start(
            activity,
            pickedFile.absolutePath,
            newFile.toString(),
            reqCode,
            false
        );
    }
}
