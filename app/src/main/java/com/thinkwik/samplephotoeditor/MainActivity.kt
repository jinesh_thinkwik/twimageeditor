package com.thinkwik.samplephotoeditor

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.kotlinpermissions.KotlinPermissions
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import com.thinkwik.thinkwikimageeditorlibrary.EditImage
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : AppCompatActivity() {

    val saveFolder = "0MyPhotoEditor"
    val PHOTO_EDITOR_REQUEST = 999
    val ei = EditImage(this@MainActivity, saveFolder, PHOTO_EDITOR_REQUEST)
    lateinit var file: File

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        openFileSelector.setOnClickListener {
            ei.pickImage(message = { message ->
                    textView.text = message
            },
                success = { file ->
                        Picasso.get()
                            .load(file)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(imageView)
                        this.file = file
                        edit.visibility = View.VISIBLE
                })
        }

        edit.setOnClickListener {
            ei.editImage(file)
        }

    }

    fun askPermissions() {
        KotlinPermissions.with(this) // where this is an FragmentActivity instance
            .permissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
            .onAccepted { permissions ->
                //List of accepted permissions

            }
            .onDenied { permissions ->
                //List of denied permissions
            }
            .onForeverDenied { permissions ->
                //List of forever denied permissions
            }
            .ask()

    }

    override fun onResume() {
        super.onResume()
        askPermissions()
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PHOTO_EDITOR_REQUEST) { // same code you used while starting
            val newFilePath = data!!.getStringExtra(ei.EXTRA_OUTPUT)
            Picasso.get()
                .load(File(newFilePath))
                .into(imageView)
        }
    }


}
